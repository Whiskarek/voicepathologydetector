package com.example.voicepathology.fragments.main

import android.animation.ValueAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.voicepathology.R
import com.google.android.material.button.MaterialButton
import com.google.android.material.textview.MaterialTextView


class ResultFragment : Fragment() {
    private companion object {
        private const val ARG_RESULT = "result"
    }

    private var mResult: Int = 0

    private lateinit var mTvResult: MaterialTextView
    private lateinit var mButtonTryAgain: MaterialButton

    private lateinit var mValueAnimator: ValueAnimator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            mResult = it.getFloat(ARG_RESULT).toInt()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mTvResult = view.findViewById(R.id.tv_result)
        mButtonTryAgain = view.findViewById(R.id.btn_try_again)
        mButtonTryAgain.setOnClickListener {
            endAnimation()
            findNavController().popBackStack()
        }
        animateResult()
    }

    private fun animateResult() {
        mValueAnimator = ValueAnimator.ofInt(0, mResult)
        mValueAnimator.duration = 2500
        mValueAnimator.addUpdateListener {
            mTvResult.text = getString(R.string.result_percentage, mValueAnimator.animatedValue)
        }
        mValueAnimator.start()
    }

    private fun endAnimation() {
        if (::mValueAnimator.isInitialized) {
            if (mValueAnimator.isRunning) {
                mValueAnimator.end()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        endAnimation()
    }
}