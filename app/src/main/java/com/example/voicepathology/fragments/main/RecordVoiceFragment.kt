package com.example.voicepathology.fragments.main

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.example.voicepathology.InfoActivity
import com.example.voicepathology.R
import com.example.voicepathology.PermissionUtils
import com.example.voicepathology.managers.RecordingManager
import com.example.voicepathology.managers.ServiceManager
import com.google.android.material.button.MaterialButton

class RecordVoiceFragment : Fragment(), ServiceManager.ResultHandler,
    RecordingManager.AudioChunkAmplitudeListener {

    private companion object {
        private const val mRequestCode = 1
    }

    private lateinit var mRecordingManager: RecordingManager
    private lateinit var mServiceManager: ServiceManager
    private lateinit var mButtonRecord: MaterialButton
    private lateinit var mButtonHowToUse: MaterialButton

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_record_voice, container, false)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mRecordingManager =
            RecordingManager(activity as AppCompatActivity)
        mRecordingManager.audioChunkAmplitudeListener = this
        mServiceManager = ServiceManager()
        mServiceManager.resultHandler = this
        mButtonRecord = view.findViewById(R.id.btn_record)
        mButtonRecord.setOnTouchListener(this::recordVoice)
        mButtonHowToUse = view.findViewById(R.id.btn_how_to_use)
        mButtonHowToUse.setOnClickListener {
            startActivity(Intent(requireContext(), InfoActivity::class.java).apply {
                putExtra("first_call", false)
                putExtra("page", 1)
            })
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        PermissionUtils.onRequestPermissionsResult(
            requestCode,
            mRequestCode,
            permissions,
            grantResults,
            this
        )
    }

    private fun recordVoice(view: View, event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            if (PermissionUtils.checkPermissionsGranted(requireActivity())) {
                mRecordingManager.startRecording()
            } else {
                PermissionUtils.requestPermissions(this, mRequestCode)
            }
            return true
        } else if (event.action == MotionEvent.ACTION_UP && mRecordingManager.isRecording) {
            mRecordingManager.stopRecording()
            animateRecordButton(1f)
            mServiceManager.sendRecord(mRecordingManager.file)
            return true
        }
        return false
    }

    private fun animateRecordButton(scale: Float) {
        mButtonRecord.animate().scaleX(scale).scaleY(scale).setDuration(100).start()
    }

    override fun onSuccess(data: String) {
        try {
            val result = data.substring(4).removeSuffix(" %").toFloat()
            findNavController().navigate(
                R.id.action_recordVoiceFragment_to_resultFragment,
                bundleOf("result" to result)
            )
        } catch (ex: Exception) {
            onFailure("")
        }
    }

    override fun onFailure(error: String) {
        findNavController().navigate(
            R.id.action_recordVoiceFragment_to_noResultFragment,
            bundleOf("error" to error)
        )
    }

    override fun onAmplitudeChanged(amplitude: Float) {
        animateRecordButton(1 + amplitude)
    }
}