package com.example.voicepathology.fragments.main

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.voicepathology.R
import com.google.android.material.button.MaterialButton
import com.google.android.material.textview.MaterialTextView
import java.lang.Exception

class NoResultFragment : Fragment() {
    private companion object {
        private const val ARG_ERROR = "error"
    }

    private lateinit var mError: String
    private lateinit var mAnimHeader: Animation
    private lateinit var mAnimInfo: Animation

    private lateinit var mTvHeader: MaterialTextView
    private lateinit var mTvInfo: MaterialTextView
    private lateinit var mBtnTryAgain: MaterialButton

    private val animationListener = object : Animation.AnimationListener {
        override fun onAnimationStart(animation: Animation?) {}

        override fun onAnimationEnd(animation: Animation?) {
            mTvInfo.visibility = View.VISIBLE
            mBtnTryAgain.visibility = View.VISIBLE
            mTvInfo.startAnimation(mAnimInfo)
            mBtnTryAgain.startAnimation(mAnimInfo)
        }

        override fun onAnimationRepeat(animation: Animation?) {}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            mError = it.getString(ARG_ERROR).orEmpty()
        }
        mAnimHeader = AnimationUtils.loadAnimation(context, R.anim.no_result_header)
        mAnimHeader.setAnimationListener(animationListener)
        mAnimInfo = AnimationUtils.loadAnimation(context, R.anim.no_result_info)
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    findNavController().popBackStack()
                }
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_no_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mTvHeader = view.findViewById(R.id.tv_no_result)
        mTvInfo = view.findViewById(R.id.tv_what_happened)
        mTvInfo.setText(getTextInfoId(mError))
        mBtnTryAgain = view.findViewById(R.id.btn_try_again)
        mBtnTryAgain.setOnClickListener {
            findNavController().popBackStack()
        }
        mTvHeader.startAnimation(mAnimHeader)
        vibrate()
    }

    private fun getTextInfoId(error: String): Int {
        return try {
            error.toInt()
            R.string.incorrect_voice_input_error
        } catch (ex: Exception) {
            R.string.network_error
        }
    }

    @Suppress("DEPRECATION")
    private fun vibrate() {
        val vibrator = requireActivity().getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        val canVibrate = vibrator.hasVibrator()
        val duration = 150L
        if (canVibrate) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                // API 26
                vibrator.vibrate(
                    VibrationEffect.createOneShot(
                        duration,
                        VibrationEffect.DEFAULT_AMPLITUDE
                    )
                )
            } else {
                // This method was deprecated in API level 26
                vibrator.vibrate(duration)
            }
        }
    }
}
