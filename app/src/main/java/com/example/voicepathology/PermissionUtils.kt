package com.example.voicepathology

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment

object PermissionUtils {
    private val permissionList = arrayOf(
        Manifest.permission.RECORD_AUDIO
    )

    private val permissionInfo =
        mapOf(permissionList[0] to (R.string.permission_record_audio_name to R.string.permission_record_audio))

    fun checkPermissionsGranted(activity: Activity): Boolean {
        return checkPermissionsGranted(activity, permissionList)
    }

    private fun checkPermissionsGranted(activity: Activity, permissions: Array<String>): Boolean {
        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(
                    activity,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return false
            }
        }
        return true
    }

    fun requestPermissions(
        fragment: Fragment,
        requestCode: Int
    ) {
        requestPermissions(fragment, permissionList, requestCode)
    }

    private fun requestPermissions(
        fragment: Fragment,
        permissions: Array<String>,
        requestCode: Int
    ) {
        val notGrantedPermissions = ArrayList<String>()
        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(
                    fragment.requireActivity(),
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                notGrantedPermissions.add(permission)
            }
        }
        if (notGrantedPermissions.isEmpty()) {
            return
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fragment.requestPermissions(notGrantedPermissions.toTypedArray(), requestCode)
        }
    }

    fun onRequestPermissionsResult(
        resultCode: Int,
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray,
        fragment: Fragment
    ) {
        when (resultCode) {
            requestCode -> {
                for (i in permissions.indices) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        val builder = AlertDialog.Builder(fragment.requireContext())
                        val strings = permissionInfo[permissions[i]]
                        builder.setTitle(strings!!.first)
                            .setMessage(strings.second)
                            .setPositiveButton(R.string.ok) { _, _ ->
                                requestPermissions(fragment, arrayOf(permissions[i]), requestCode)
                            }
                            .setNegativeButton(R.string.cancel) { _, _ -> }
                            .create()
                            .show()
                    }
                }
            }
        }
    }
}