package com.example.voicepathology.managers

import android.util.Log
import com.example.voicepathology.corpus.services.ServiceGenerator
import com.example.voicepathology.corpus.services.VoicePathologyService
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Callback
import java.io.File

class ServiceManager : Callback<String> {
    interface ResultHandler {
        fun onSuccess(data: String)

        fun onFailure(error: String)
    }

    private companion object {
        private val LOG_TAG = ServiceManager::class.simpleName
    }

    lateinit var resultHandler: ResultHandler

    fun sendRecord(file: File) {
        val service = ServiceGenerator.createService(VoicePathologyService::class.java)
        val requestFile = RequestBody.create(
            MediaType.parse("audio/wav"),
            file
        )
        val data = MultipartBody.Part.createFormData("audio", file.name, requestFile)
        val call = service.send(data)
        call.enqueue(this)
    }

    override fun onResponse(
        call: retrofit2.Call<String>,
        response: retrofit2.Response<String>
    ) {
        val code = response.code()
        if (code == 200) { // OK
            val result = response.body()!!
            Log.d(LOG_TAG, result)
            if (::resultHandler.isInitialized) {
                resultHandler.onSuccess(result)
            }
        } else {
            Log.e(LOG_TAG, response.toString())
            if (::resultHandler.isInitialized) {
                resultHandler.onFailure(code.toString())
            }
        }
    }

    override fun onFailure(call: retrofit2.Call<String>, t: Throwable) {
        val message = t.message.orEmpty()
        Log.d(LOG_TAG, message)
        if (::resultHandler.isInitialized) {
            resultHandler.onFailure(message)
        }
    }
}