package com.example.voicepathology.managers

import android.media.AudioFormat
import android.media.MediaRecorder
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import omrecorder.*
import java.io.File


class RecordingManager(activity: AppCompatActivity) {
    interface AudioChunkAmplitudeListener {
        fun onAmplitudeChanged(amplitude: Float)
    }

    private companion object {
        private val LOG_TAG = RecordingManager::class.simpleName
        private const val FILE_NAME = "tmp.wav"
    }

    private lateinit var mAudioRecorder: Recorder

    lateinit var audioChunkAmplitudeListener: AudioChunkAmplitudeListener

    var isRecording = false
        private set

    val file = File(activity.externalCacheDir, FILE_NAME)

    fun startRecording() {
        isRecording = true
        initializeRecorder()
        mAudioRecorder.startRecording()
        Log.d(LOG_TAG, "recording started")
    }

    fun stopRecording() {
        isRecording = false
        mAudioRecorder.stopRecording()
        Log.d(LOG_TAG, "recording finished")
    }

    private fun initializeRecorder() {
        mAudioRecorder = OmRecorder.wav(
            PullTransport.Noise(
                getMicroConfiguration(),
                PullTransport.OnAudioChunkPulledListener { audioChunk ->
                    if (::audioChunkAmplitudeListener.isInitialized) {
                        val amplitude = (audioChunk.maxAmplitude() / 200.0).toFloat()
                        if (amplitude >= 0) {
                            audioChunkAmplitudeListener.onAmplitudeChanged(amplitude)
                        }
                    }
                },
                WriteAction.Default(),
                Recorder.OnSilenceListener { },
                2
            ), file
        )
    }

    private fun getMicroConfiguration(): PullableSource {
        return PullableSource.AutomaticGainControl(
            PullableSource.NoiseSuppressor(
                PullableSource.Default(
                    AudioRecordConfig.Default(
                        MediaRecorder.AudioSource.MIC, AudioFormat.ENCODING_PCM_16BIT,
                        AudioFormat.CHANNEL_IN_MONO, 44100
                    )
                )
            )
        )
    }
}