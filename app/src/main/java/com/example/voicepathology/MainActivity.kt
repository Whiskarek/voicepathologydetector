package com.example.voicepathology

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    private companion object {
        private const val SP_FIRST_LOAD = "first_load"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (isFirstLoad()) {
            startActivityForResult(Intent(this, InfoActivity::class.java), -1)
            setFirstLoad(false)
        }
        setContentView(R.layout.activity_main)
    }

    private fun isFirstLoad(): Boolean {
        val sharedPreferences = getSharedPreferences("Default", Context.MODE_PRIVATE)
        return sharedPreferences.getBoolean(SP_FIRST_LOAD, true)
    }

    private fun setFirstLoad(status: Boolean) {
        val sharedPreferences = getSharedPreferences("Default", Context.MODE_PRIVATE)
        sharedPreferences.edit().putBoolean(SP_FIRST_LOAD, status).apply()
    }
}