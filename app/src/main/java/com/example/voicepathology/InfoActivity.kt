package com.example.voicepathology

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.example.voicepathology.fragments.info.DefaultFragment
import com.google.android.material.floatingactionbutton.FloatingActionButton

class InfoActivity : AppCompatActivity() {
    private companion object {
        private const val ARG_FIRST_CALL = "first_call"
        private const val ARG_PAGE = "page"
    }

    private var mFirstCall: Boolean = true
    private var mPage: Int = 0

    private lateinit var mPagerAdapter: PagerAdapter
    private lateinit var mViewPager: ViewPager2
    private lateinit var mButtonNext: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        mFirstCall = intent.getBooleanExtra(ARG_FIRST_CALL, true)
        mPage = intent.getIntExtra(ARG_PAGE, 0)
        mButtonNext = findViewById(R.id.btn_next)
        mButtonNext.setOnClickListener {
            if (mViewPager.currentItem == mPagerAdapter.itemCount - 1) {
                finish()
            } else {
                mViewPager.currentItem = mViewPager.currentItem + 1
            }
        }
        mPagerAdapter =
            PagerAdapter(getFragmentList(), supportFragmentManager, lifecycle)
        mViewPager = findViewById(R.id.vp_info)
        mViewPager.adapter = mPagerAdapter
        mViewPager.setCurrentItem(mPage, false)
    }

    private fun getFragmentList(): List<Fragment> {
        return listOf(
            DefaultFragment(R.layout.fragment_info_welcome),
            DefaultFragment(R.layout.fragment_info_description)
        )
    }

    override fun onBackPressed() {
        val currentPage = mViewPager.currentItem
        if (!mFirstCall && currentPage == 0) {
            super.onBackPressed()
        } else if (currentPage != 0) {
            mViewPager.setCurrentItem(currentPage - 1, true)
        } else if (mFirstCall && currentPage == 0) {
            finish()
        }
    }

}

class PagerAdapter(
    private val mFragmentList: List<Fragment>,
    mFragmentManager: FragmentManager,
    mLifecycle: Lifecycle
) :
    FragmentStateAdapter(mFragmentManager, mLifecycle) {

    override fun createFragment(position: Int): Fragment = mFragmentList[position]

    override fun getItemCount(): Int = mFragmentList.size
}