package com.example.voicepathology.corpus.services

import retrofit2.Retrofit

object ServiceGenerator {
    private const val BASE_URL = "https://corpus.by/"

    private val builder = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(ToStringConverterFactory())

    private val retrofit = builder.build()

    fun <S> createService(serviceClass: Class<S>?): S {
        return retrofit.create(serviceClass)
    }
}