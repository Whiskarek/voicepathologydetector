package com.example.voicepathology.corpus.services

import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface VoicePathologyService {
    @Multipart
    @POST("VoicePathologyDetector/api")
    fun send(
        @Part file: MultipartBody.Part
    ): Call<String>
}